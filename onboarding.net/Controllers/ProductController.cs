﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using onboarding.net.Data;
using onboarding.net.Models;

namespace onboarding.net.Controllers
{
    public class ProductController : Controller
    {
        //request object dari ShopContext
        private readonly ShopContext _db;

        public ProductController(ShopContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexAdmin(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name" : "";
            ViewBag.LastUpdateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var products = from p in _db.Products select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name":
                    products = products.OrderByDescending(p => p.Name);
                    break;
                case "Date":
                    products = products.OrderBy(p => p.LastUpdateTime);
                    break;
                case "date_desc":
                    products = products.OrderByDescending(p => p.LastUpdateTime);
                    break;
                default:
                    products = products.OrderBy(p => p.Name);
                    break;
            }
            return View(products.ToList());
        }

        public IActionResult Create()
        {
            PopulateShopsDropDownList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Products obj)
        {
            if (ModelState.IsValid)
            {
                _db.Products.Add(obj);
                _db.SaveChanges();
                TempData["success"] = "Product created successfully!";
                return RedirectToAction("IndexAdmin");
            }
            PopulateShopsDropDownList();
            return View(obj);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var productFromDb = _db.Products.Find(id);

            if (productFromDb == null)
            {
                return NotFound();
            }
            return View(productFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Products obj)
        {
            if (ModelState.IsValid)
            {
                _db.Products.Update(obj);
                _db.SaveChanges();
                TempData["success"] = "Product edit successfully!";
                return RedirectToAction("IndexAdmin");
            }
            return View(obj);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var obj = _db.Products.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Products.Remove(obj);
            _db.SaveChanges();
            TempData["success"] = "Products delete successfully!";
            return RedirectToAction("IndexAdmin");
        }


        private void PopulateShopsDropDownList()
        {
            var shopsQuery = from d in _db.Shops orderby d.Name select d;

            ViewBag.ShopId = new SelectList(shopsQuery, "Id", "Name");
        }
    }
}
