﻿using Microsoft.AspNetCore.Mvc;
using onboarding.net.Data;
using onboarding.net.Models;

namespace onboarding.net.Controllers
{
    public class ShopController : Controller
    {
        //request object dari ShopContext
        private readonly ShopContext _db;

        public ShopController(ShopContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexAdmin(string searchString)
        {
            var shops = from s in _db.Shops select s;

            if (!string.IsNullOrEmpty(searchString))
            {
                IEnumerable<Shop> objLocationShopLists = shops.Where(s => s.Location!.Contains(searchString));
                return View(objLocationShopLists);
            }

            IEnumerable<Shop> objShopList = _db.Shops;
            return View(objShopList);
        }

        //GET
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Shop obj)
        {
            if (ModelState.IsValid)
            {
                _db.Shops.Add(obj);
                _db.SaveChanges();
                TempData["success"] = "Category created successfully!";
                return RedirectToAction("IndexAdmin");
            }
            return View(obj);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var shopFromDb = _db.Shops.Find(id);

            if (shopFromDb == null)
            {
                return NotFound();
            }
            return View(shopFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Shop obj)
        {
            if (ModelState.IsValid)
            {
                _db.Shops.Update(obj);
                _db.SaveChanges();
                TempData["success"] = "Category edit successfully!";
                return RedirectToAction("IndexAdmin");
            }
            return View(obj);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var obj = _db.Shops.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Shops.Remove(obj);
            _db.SaveChanges();
            TempData["success"] = "Category delete successfully!";
            return RedirectToAction("IndexAdmin");
        }
    }
}
