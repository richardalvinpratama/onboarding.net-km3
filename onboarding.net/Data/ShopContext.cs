﻿using Microsoft.EntityFrameworkCore;
using onboarding.net.Models;
//using System.Data.Entity;
//using System.Data.Entity.ModelConfiguration.Conventions;

namespace onboarding.net.Data
{
    public class ShopContext : DbContext
    {
        public ShopContext(DbContextOptions<ShopContext> options) : base(options)
        {

        }

        //buat tabel di database
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Products> Products { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //}
    }
}
