﻿using System.ComponentModel.DataAnnotations;

namespace onboarding.net.Models
{
    public class Shop
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Location { get; set; }   
        
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        public virtual ICollection<Products> Products { get; set; }
    }
}
