﻿using System.ComponentModel.DataAnnotations;

namespace onboarding.net.Models
{
    public class Products
    {
        [Key]
        public int Id { get; set; }
        public int ShopId { get; set; } //will recognize as foreign id
        [Required]
        public string Name { get; set; }
        public string Desc { get; set; }
        public int Stock { get; set; }
        public int Price { get; set; }
        public DateTime LastUpdateTime { get; set; } = DateTime.Now;
    }
}
