﻿using Microsoft.EntityFrameworkCore;
using onboarding.net.Data;

namespace onboarding.net.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ShopContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ShopContext>>()))
            {
                if (context.Shops.Any())
                {
                    return;
                }

                context.Shops.AddRange(
                    new Shop
                    {
                        Name = "ClownRent",
                        Location = "Jakarta",
                    },

                    new Shop
                    {
                        Name = "PartyRent",
                        Location = "Jambi",
                    },

                    new Shop
                    {
                        Name = "MickeyRent",
                        Location = "Riau",
                    }

                    );
                context.SaveChanges();


            }
        }
    }
}
